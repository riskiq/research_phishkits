# Research PhishKits

WARNING: THESE ARE LIVE PHISHKITS BE CAREFUL RUNNING CODE OR INTERACTING WITH THE KITS!!!

Welcome to the PhishKit repo! This repo serves as a centralized place for phishing kits discovered by the research team for analysis. 

## Updates

Updates to this repo are added as often as time permits.

Due to the size of all the resources for these kits, we will rotate months, else this repository will likely approach a few gig.

## Submissions

Find a cool PhishKit? Create a PR with the kit and md5 for the kit. 

## Maintainers

- Adam "The Other Adam" Castleman


